from hashlib import sha256 as H
import hmac

# For the reference see RFC 2104:
#
# Krawczyk H., Bellare M., Canetti R.
# HMAC: Keyed-Hashing for Message Authentication
# 
# https://tools.ietf.org/html/rfc2104

def xor(data, byte, block_size):
	pad_size = (-len(data)) % block_size
	return bytes(c ^ byte for c in data) + bytes(byte for _ in range(pad_size))

def HMAC(key, message):
	ipad_byte = 0x36
	opad_byte = 0x5C
	block_size = 64 # SHA256 block size is 64 bytes
	return H(xor(key, opad_byte, block_size) + H(xor(key, ipad_byte, block_size) + message).digest()).hexdigest()

key = b'61=:eh@W\x0c\x17x\x97.m|m\xb6\x15MH\x88~G}j=r\xe9\xf5\xb2\xdf|'

message = b'hello world'

h1 = HMAC(key, message)
print(h1)

### TEST
h2 = hmac.new(key, message, H).hexdigest()
print(h2)

assert h1 == h2