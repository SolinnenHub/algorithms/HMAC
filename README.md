# HMAC (RFC 2104)

The definition of HMAC requires a cryptographic hash function, which we denote by H, and a secret key K. We assume H to be a cryptographic hash function where data is hashed by iterating a basic compression function on blocks of data.   We denote by B the byte-length of such blocks (B=64 for all the above mentioned examples of hash functions), and by L the byte-length of hash outputs (L=16 for MD5, L=20 for SHA-1).  The authentication key K can be of any length up to B, the block length of the hash function.  Applications that use keys longer than B bytes will first hash the key using H and then use the resultant L byte string as the actual key to HMAC. In any case the minimal recommended length for K is L bytes (as the hash output length). See section 3 for more information on keys.

We define two fixed and different strings ipad and opad as follows (the 'i' and 'o' are mnemonics for inner and outer):

    ipad = the byte 0x36 repeated B times
    opad = the byte 0x5C repeated B times.

To compute HMAC over the data `text' we perform

    H(K XOR opad, H(K XOR ipad, text))

---

For the reference see RFC 2104:

> Krawczyk H., Bellare M., Canetti R.
> HMAC: Keyed-Hashing for Message Authentication
> [https://tools.ietf.org/html/rfc2104](https://tools.ietf.org/html/rfc2104)